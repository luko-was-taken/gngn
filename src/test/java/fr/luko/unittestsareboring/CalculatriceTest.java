package fr.luko.unittestsareboring;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class CalculatriceTest {

    Calculatrice calculatrice;

    @BeforeEach
    void setUp() {
        this.calculatrice = new Calculatrice();
    }

    @Test
    @DisplayName("L'addition doit retourner le resultat de L'ADDITION des deux nombres")
    void testAddition() {
        Random random = new Random();
        int[] array0 = random.ints(1000, 10,100000).toArray();
        int[] array1 = random.ints(1000, 10,100000).toArray();
        for (int i = 0; i < array0.length; i++)
            assertEquals(array0[i]+array1[i], calculatrice.addition(array0[i],array1[i]));

    }

}