package fr.luko.unittestsareboring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnitTestsAreBoringApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnitTestsAreBoringApplication.class, args);
    }

}
